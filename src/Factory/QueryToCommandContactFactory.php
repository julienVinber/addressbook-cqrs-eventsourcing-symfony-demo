<?php

namespace App\Factory;

use App\Entity\Command\Contact\CommandContactDelete;
use App\Entity\Command\Contact\CommandContactEditAddress;
use App\Entity\Command\Contact\CommandContactEditEmail;
use App\Entity\Command\Contact\CommandContactEditFamillyName;
use App\Entity\Command\Contact\CommandContactEditGivenName;
use App\Entity\Command\Contact\CommandContactEditName;
use App\Entity\Command\Contact\CommandContactEditPhone;
use App\Entity\Query\Contact;

class QueryToCommandContactFactory
{
    public static function toContactEditGivenName(Contact $contact): CommandContactEditGivenName
    {
        $contactEdit = new CommandContactEditGivenName();
        $contactEdit->setId($contact->getId());
        $contactEdit->setGivenName($contact->getGiveName() ?? '');

        return $contactEdit;
    }

    public static function toContactEditFamillyName(Contact $contact): CommandContactEditFamillyName
    {
        $contactEdit = new CommandContactEditFamillyName();
        $contactEdit->setId($contact->getId());
        $contactEdit->setFamillyName($contact->getFamillyName() ?? '');

        return $contactEdit;
    }

    public static function toContactEditName(Contact $contact): CommandContactEditName
    {
        $contactEdit = new CommandContactEditName();
        $contactEdit->setId($contact->getId());
        $contactEdit->setName($contact->getName());

        return $contactEdit;
    }

    public static function toContactEditAddress(Contact $contact): CommandContactEditAddress
    {
        $contactEdit = new CommandContactEditAddress();
        $contactEdit->setId($contact->getId());
        $contactEdit->setAddress($contact->getAddress() ?? '');
        $contactEdit->setPostalCode($contact->getPostalCode() ?? '');
        $contactEdit->setCity($contact->getCity() ?? '');

        return $contactEdit;
    }

    public static function toContactEditPhone(Contact $contact): CommandContactEditPhone
    {
        $contactEdit = new CommandContactEditPhone();
        $contactEdit->setId($contact->getId());
        $contactEdit->setPhone($contact->getPhone() ?? '');

        return $contactEdit;
    }

    public static function toContactEditEmail(Contact $contact): CommandContactEditEmail
    {
        $contactEdit = new CommandContactEditEmail();
        $contactEdit->setId($contact->getId());
        $contactEdit->setEmail($contact->getEmail() ?? '');

        return $contactEdit;
    }

    public static function toContactDelete(Contact $contact): CommandContactDelete
    {
        $contactDelete = new CommandContactDelete();
        $contactDelete->setId($contact->getId());

        return $contactDelete;
    }
}
