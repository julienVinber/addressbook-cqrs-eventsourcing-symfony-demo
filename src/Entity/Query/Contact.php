<?php

namespace App\Entity\Query;

use App\CQRS\Entity\Query\QueryEntityInterface;
use App\Repository\Query\ContactRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: ContactRepository::class)]
class Contact implements QueryEntityInterface
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    private Uuid $id;

    #[ORM\Column(type: 'string')]
    private string $name;

    #[ORM\Column(type: 'string')]
    private string $sortName;

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $giveName = null;

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $famillyName = null;

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $address = null;

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $postalCode = null;

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $city = null;

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $email = null;

    #[ORM\Column(type: 'string', nullable: true)]
    private ?string $phone = null;

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(Uuid $id): Contact
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Contact
    {
        $this->name = $name;

        return $this;
    }

    public function getSortName(): string
    {
        return $this->sortName;
    }

    public function setSortName(string $sortName): Contact
    {
        $this->sortName = $sortName;

        return $this;
    }

    public function getGiveName(): ?string
    {
        return $this->giveName;
    }

    public function setGiveName(?string $giveName): Contact
    {
        $this->giveName = $giveName;

        return $this;
    }

    public function getFamillyName(): ?string
    {
        return $this->famillyName;
    }

    public function setFamillyName(?string $famillyName): Contact
    {
        $this->famillyName = $famillyName;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): Contact
    {
        $this->address = $address;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): Contact
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): Contact
    {
        $this->city = $city;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): Contact
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): Contact
    {
        $this->phone = $phone;

        return $this;
    }
}
