<?php

namespace App\Entity\Event\Contact;

use App\CQRS\Entity\Event\EventDataInterface;
use Symfony\Component\Uid\Uuid;

class EventContactDelete implements EventDataInterface
{
    private Uuid $id;

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(Uuid $id): EventContactDelete
    {
        $this->id = $id;

        return $this;
    }
}
