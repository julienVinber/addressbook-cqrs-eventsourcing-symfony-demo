<?php

namespace App\Entity\Event\Contact;

use App\CQRS\Entity\Event\EventDataInterface;
use Symfony\Component\Uid\Uuid;

class EventContactEditPhone implements EventDataInterface
{
    private Uuid $id;

    private string $phone;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(Uuid $id): EventContactEditPhone
    {
        $this->id = $id;

        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): EventContactEditPhone
    {
        $this->phone = $phone;

        return $this;
    }
}
