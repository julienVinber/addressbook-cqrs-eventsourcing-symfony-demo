<?php

namespace App\Entity\Event\Contact;

use App\CQRS\Entity\Event\EventDataInterface;
use Symfony\Component\Uid\Uuid;

class EventContactEditFamillyName implements EventDataInterface
{
    private Uuid $id;

    private string $famillyName;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(Uuid $id): EventContactEditFamillyName
    {
        $this->id = $id;

        return $this;
    }

    public function getFamillyName(): string
    {
        return $this->famillyName;
    }

    public function setFamillyName(string $famillyName): EventContactEditFamillyName
    {
        $this->famillyName = $famillyName;

        return $this;
    }
}
