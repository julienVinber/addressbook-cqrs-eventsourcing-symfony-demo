<?php

namespace App\Entity\Event\Contact;

use App\CQRS\Entity\Event\EventDataInterface;
use Symfony\Component\Uid\Uuid;

class EventContactCreate implements EventDataInterface
{
    private Uuid $id;

    private string $name;

    private ?string $givenName = null;

    private ?string $famillyName = null;

    private ?string $address = null;

    private ?string $postalCode = null;

    private ?string $city = null;

    private ?string $phone = null;

    private ?string $email = null;

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(Uuid $id): EventContactCreate
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): EventContactCreate
    {
        $this->name = $name;

        return $this;
    }

    public function getGivenName(): ?string
    {
        return $this->givenName;
    }

    public function setGivenName(?string $givenName): EventContactCreate
    {
        $this->givenName = $givenName;

        return $this;
    }

    public function getFamillyName(): ?string
    {
        return $this->famillyName;
    }

    public function setFamillyName(?string $famillyName): EventContactCreate
    {
        $this->famillyName = $famillyName;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): EventContactCreate
    {
        $this->address = $address;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): EventContactCreate
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): EventContactCreate
    {
        $this->city = $city;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): EventContactCreate
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): EventContactCreate
    {
        $this->email = $email;

        return $this;
    }
}
