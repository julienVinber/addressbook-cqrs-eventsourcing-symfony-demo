<?php

namespace App\Entity\Event\Contact;

use App\CQRS\Entity\Event\EventDataInterface;
use Symfony\Component\Uid\Uuid;

class EventContactEditName implements EventDataInterface
{
    private Uuid $id;

    private string $name;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(Uuid $id): EventContactEditName
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): EventContactEditName
    {
        $this->name = $name;

        return $this;
    }
}
