<?php

namespace App\Entity\Event\Contact;

use App\CQRS\Entity\Event\EventDataInterface;
use Symfony\Component\Uid\Uuid;

class EventContactEditEmail implements EventDataInterface
{
    private Uuid $id;

    private string $email;

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(Uuid $id): EventContactEditEmail
    {
        $this->id = $id;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): EventContactEditEmail
    {
        $this->email = $email;

        return $this;
    }
}
