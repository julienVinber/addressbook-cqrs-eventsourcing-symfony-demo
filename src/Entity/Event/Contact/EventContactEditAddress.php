<?php

namespace App\Entity\Event\Contact;

use App\CQRS\Entity\Event\EventDataInterface;
use Symfony\Component\Uid\Uuid;

class EventContactEditAddress implements EventDataInterface
{
    private Uuid $id;

    private string $address;

    private string $postalCode;

    private string $city;

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(Uuid $id): EventContactEditAddress
    {
        $this->id = $id;

        return $this;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress(string $address): EventContactEditAddress
    {
        $this->address = $address;

        return $this;
    }

    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): EventContactEditAddress
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): EventContactEditAddress
    {
        $this->city = $city;

        return $this;
    }
}
