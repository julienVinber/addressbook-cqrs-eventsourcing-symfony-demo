<?php

namespace App\Entity\Event\Contact;

use App\CQRS\Entity\Event\EventDataInterface;
use Symfony\Component\Uid\Uuid;

class EventContactEditGivenName implements EventDataInterface
{
    private Uuid $id;

    private string $givenName;

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(Uuid $id): EventContactEditGivenName
    {
        $this->id = $id;

        return $this;
    }

    public function getGivenName(): string
    {
        return $this->givenName;
    }

    public function setGivenName(string $givenName): EventContactEditGivenName
    {
        $this->givenName = $givenName;

        return $this;
    }
}
