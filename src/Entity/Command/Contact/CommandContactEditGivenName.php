<?php

namespace App\Entity\Command\Contact;

use App\CQRS\Entity\Command\CommandEntityInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

class CommandContactEditGivenName implements CommandEntityInterface
{
    #[Assert\Uuid]
    private Uuid $id;

    #[Assert\Length(min: 0, max: 64)]
    private string $givenName;

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(Uuid $id): CommandContactEditGivenName
    {
        $this->id = $id;

        return $this;
    }

    public function getGivenName(): string
    {
        return $this->givenName;
    }

    public function setGivenName(string $givenName): CommandContactEditGivenName
    {
        $this->givenName = $givenName;

        return $this;
    }
}
