<?php

namespace App\Entity\Command\Contact;

use App\CQRS\Entity\Command\CommandEntityInterface;
use Symfony\Component\Validator\Constraints as Assert;

class CommandContactCreate implements CommandEntityInterface
{
    #[Assert\Length(min: 2)]
    private string $name;

    #[Assert\Length(min: 0, max: 64)]
    private ?string $givenName = null;

    #[Assert\Length(min: 0, max: 64)]
    private ?string $famillyName = null;

    #[Assert\Length(min: 5)]
    private ?string $address = null;

    #[Assert\Length(min: 5, max: 5)]
    private ?string $postalCode = null;

    #[Assert\Length(min: 2)]
    private ?string $city = null;

    #[Assert\Length(min: 10, max: 20)]
    private ?string $phone = null;

    #[Assert\Email]
    private ?string $email = null;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): CommandContactCreate
    {
        $this->name = $name;

        return $this;
    }

    public function getGivenName(): ?string
    {
        return $this->givenName;
    }

    public function setGivenName(?string $givenName): CommandContactCreate
    {
        $this->givenName = $givenName;

        return $this;
    }

    public function getFamillyName(): ?string
    {
        return $this->famillyName;
    }

    public function setFamillyName(?string $famillyName): CommandContactCreate
    {
        $this->famillyName = $famillyName;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): CommandContactCreate
    {
        $this->address = $address;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): CommandContactCreate
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): CommandContactCreate
    {
        $this->city = $city;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): CommandContactCreate
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): CommandContactCreate
    {
        $this->email = $email;

        return $this;
    }
}
