<?php

namespace App\Entity\Command\Contact;

use App\CQRS\Entity\Command\CommandEntityInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

class CommandContactEditPhone implements CommandEntityInterface
{
    #[Assert\Uuid]
    private Uuid $id;

    #[Assert\Length(min: 10, max: 20)]
    private string $phone;

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(Uuid $id): CommandContactEditPhone
    {
        $this->id = $id;

        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): CommandContactEditPhone
    {
        $this->phone = $phone;

        return $this;
    }
}
