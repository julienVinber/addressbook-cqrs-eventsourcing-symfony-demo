<?php

namespace App\Entity\Command\Contact;

use App\CQRS\Entity\Command\CommandEntityInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

class CommandContactEditFamillyName implements CommandEntityInterface
{
    #[Assert\Uuid]
    private Uuid $id;

    #[Assert\Length(min: 0, max: 64)]
    private string $famillyName;

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(Uuid $id): CommandContactEditFamillyName
    {
        $this->id = $id;

        return $this;
    }

    public function getFamillyName(): string
    {
        return $this->famillyName;
    }

    public function setFamillyName(string $famillyName): CommandContactEditFamillyName
    {
        $this->famillyName = $famillyName;

        return $this;
    }
}
