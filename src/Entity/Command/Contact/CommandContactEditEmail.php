<?php

namespace App\Entity\Command\Contact;

use App\CQRS\Entity\Command\CommandEntityInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

class CommandContactEditEmail implements CommandEntityInterface
{
    #[Assert\Uuid]
    private Uuid $id;

    #[Assert\Email]
    private string $email;

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(Uuid $id): CommandContactEditEmail
    {
        $this->id = $id;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): CommandContactEditEmail
    {
        $this->email = $email;

        return $this;
    }
}
