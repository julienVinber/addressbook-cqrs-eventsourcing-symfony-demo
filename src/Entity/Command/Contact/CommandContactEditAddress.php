<?php

namespace App\Entity\Command\Contact;

use App\CQRS\Entity\Command\CommandEntityInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

class CommandContactEditAddress implements CommandEntityInterface
{
    #[Assert\Uuid]
    private Uuid $id;

    #[Assert\Length(min: 5)]
    private string $address;

    #[Assert\Length(min: 5, max: 5)]
    private string $postalCode;

    #[Assert\Length(min: 2)]
    private string $city;

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(Uuid $id): CommandContactEditAddress
    {
        $this->id = $id;

        return $this;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress(string $address): CommandContactEditAddress
    {
        $this->address = $address;

        return $this;
    }

    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): CommandContactEditAddress
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): CommandContactEditAddress
    {
        $this->city = $city;

        return $this;
    }
}
