<?php

namespace App\Entity\Command\Contact;

use App\CQRS\Entity\Command\CommandEntityInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

class CommandContactDelete implements CommandEntityInterface
{
    #[Assert\Uuid]
    private Uuid $id;

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function setId(Uuid $id): CommandContactDelete
    {
        $this->id = $id;

        return $this;
    }
}
