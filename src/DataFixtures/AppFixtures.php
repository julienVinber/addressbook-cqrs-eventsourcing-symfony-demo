<?php

namespace App\DataFixtures;

use App\CQRS\Factory\EventEntityFactory;
use App\CQRS\Repository\Event\EventEntityRepository;
use App\Entity\Event\Contact\EventContactCreate;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Uid\Uuid;

class AppFixtures extends Fixture
{
    public function __construct(private readonly EventEntityRepository $eventEntityRepository)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 100; $i++) {
            $firstName = $faker->firstName();
            $lastName = $faker->lastName();

            $this->eventEntityRepository->add(EventEntityFactory::createEvent((new EventContactCreate())
                ->setId(Uuid::v7())
                ->setName($firstName.' '.$lastName)
                ->setGivenName($firstName)
                ->setFamillyName($lastName)
                ->setAddress($faker->streetAddress())
                ->setPostalCode($faker->postcode())
                ->setCity($faker->city())
                ->setEmail($faker->safeEmail())
                ->setPhone($faker->phoneNumber())
            ), false);
        }

        $manager->flush();
    }
}
