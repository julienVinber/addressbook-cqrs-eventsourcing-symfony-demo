<?php

namespace App\Controller\Query\Contact;

use App\CQRS\Controller\AbstractQueryController;
use App\Repository\Query\ContactRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Attribute\Route;

#[AsController]
#[Route('/', name: 'home')]
class QueryContactAllController extends AbstractQueryController
{
    public function __invoke(ContactRepository $contactRepository): Response
    {
        $contacts = $contactRepository->getAll();

        return $this->render('home.html.twig', [
            'contacts' => $contacts,
        ]);
    }
}
