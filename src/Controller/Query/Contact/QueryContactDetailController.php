<?php

namespace App\Controller\Query\Contact;

use App\CQRS\Controller\AbstractQueryController;
use App\Entity\Query\Contact;
use App\Factory\QueryToCommandContactFactory;
use App\Form\Type\Contact\ContactDeleteType;
use App\Form\Type\Contact\ContactEditAddressType;
use App\Form\Type\Contact\ContactEditEmailType;
use App\Form\Type\Contact\ContactEditFamillyNameType;
use App\Form\Type\Contact\ContactEditGivenNameType;
use App\Form\Type\Contact\ContactEditNameType;
use App\Form\Type\Contact\ContactEditPhoneType;
use App\Repository\Query\ContactRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Attribute\Route;

#[AsController]
#[Route('/contact/{id}', name: 'contact_detail')]
class QueryContactDetailController extends AbstractQueryController
{
    public function __invoke(ContactRepository $contactRepository, Contact $contact, Request $request): Response
    {
        return $this->render('contact\detail.html.twig', [
            'contact' => $contact,
            'formGivenName' => $this->createForm(ContactEditGivenNameType::class, QueryToCommandContactFactory::toContactEditGivenName($contact))->handleRequest($request)->createView(),
            'formFamillyName' => $this->createForm(ContactEditFamillyNameType::class, QueryToCommandContactFactory::toContactEditFamillyName($contact))->handleRequest($request)->createView(),
            'formName' => $this->createForm(ContactEditNameType::class, QueryToCommandContactFactory::toContactEditName($contact))->handleRequest($request)->createView(),
            'formAddress' => $this->createForm(ContactEditAddressType::class, QueryToCommandContactFactory::toContactEditAddress($contact))->handleRequest($request)->createView(),
            'formPhone' => $this->createForm(ContactEditPhoneType::class, QueryToCommandContactFactory::toContactEditPhone($contact))->handleRequest($request)->createView(),
            'formEmail' => $this->createForm(ContactEditEmailType::class, QueryToCommandContactFactory::toContactEditEmail($contact))->handleRequest($request)->createView(),
            'formDelete' => $this->createForm(ContactDeleteType::class, QueryToCommandContactFactory::toContactDelete($contact))->handleRequest($request)->createView(),
        ]);
    }
}
