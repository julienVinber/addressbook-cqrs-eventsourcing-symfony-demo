<?php

namespace App\Controller\Query\Contact;

use App\CQRS\Controller\AbstractQueryController;
use App\Entity\Command\Contact\CommandContactCreate;
use App\Form\Type\Contact\ContactCreateType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Attribute\Route;

#[AsController]
#[Route('/contact/create/form', name: 'contact_create_form')]
class QueryContactCreateFormController extends AbstractQueryController
{
    public function __invoke(Request $request): Response
    {
        return $this->render('contact\create.html.twig', [
            'form' => $this->createForm(ContactCreateType::class, new CommandContactCreate())->handleRequest($request)->createView(),
        ]);
    }
}
