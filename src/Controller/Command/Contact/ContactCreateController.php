<?php

namespace App\Controller\Command\Contact;

use App\Controller\Query\Contact\QueryContactCreateFormController;
use App\CQRS\Attribute\AsCommandController;
use App\CQRS\Controller\AbstractCommandController;
use App\CQRS\Entity\Command\CommandEntityInterface;
use App\CQRS\Entity\Event\EventDataInterface;
use App\CQRS\Entity\Event\EventEntity;
use App\Entity\Command\Contact\CommandContactCreate;
use App\Entity\Event\Contact\EventContactCreate;
use Exception;
use Override;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Uid\Uuid;

#[AsCommandController(commandClassName: CommandContactCreate::class, formTypeClassName: CommandContactCreate::class)]
#[Route('/contact/create', name: 'contact_create', methods: 'POST')]
class ContactCreateController extends AbstractCommandController
{
    /** @param CommandContactCreate $commandEntity */
    #[Override] protected function toEventDataEntity(CommandEntityInterface $commandEntity): EventDataInterface
    {
        return (new EventContactCreate())
            ->setId(Uuid::v7())
            ->setName($commandEntity->getName())
            ->setGivenName($commandEntity->getGivenName())
            ->setFamillyName($commandEntity->getFamillyName())
            ->setAddress($commandEntity->getAddress())
            ->setPostalCode($commandEntity->getPostalCode())
            ->setCity($commandEntity->getCity())
            ->setEmail($commandEntity->getEmail())
            ->setPhone($commandEntity->getEmail());
    }

    #[Override] protected function getResponse(EventEntity $eventEntity): Response
    {
        /** @var EventContactCreate $eventData */
        $eventData = $eventEntity->getEventData();

        return $this->redirectToRoute('contact_detail', ['id' => $eventData->getId()]);
    }

    /**
     * @throws Exception
     */
    #[Override] protected function onFormError(Request $request, FormInterface $form, HttpKernelInterface $httpKernel): Response
    {
        return $this->handelSubController($request, $form, $httpKernel, QueryContactCreateFormController::class);
    }
}
