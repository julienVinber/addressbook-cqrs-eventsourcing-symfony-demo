<?php

namespace App\Controller\Command\Contact;

use App\Controller\Query\Contact\QueryContactDetailController;
use App\CQRS\Attribute\AsCommandController;
use App\CQRS\Controller\AbstractCommandController;
use App\CQRS\Entity\Command\CommandEntityInterface;
use App\CQRS\Entity\Event\EventDataInterface;
use App\CQRS\Entity\Event\EventEntity;
use App\Entity\Command\Contact\CommandContactEditName;
use App\Entity\Event\Contact\EventContactEditName;
use App\Form\Type\Contact\ContactEditNameType;
use App\Security\Voter\ContactExistVoter;
use Exception;
use Override;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Attribute\Route;

#[AsCommandController(commandClassName: CommandContactEditName::class, formTypeClassName: ContactEditNameType::class)]
#[Route(path: '/contact/edit/name', name: 'contact_edit_name', methods: 'POST')]
class ContactEditNameController extends AbstractCommandController
{
    /** @param CommandContactEditName $commandEntity */
    #[Override] protected function isAuthorized(CommandEntityInterface $commandEntity): bool
    {
        return $this->isGranted(ContactExistVoter::CONTACT_EXIST, $commandEntity->getId());
    }

    /** @param CommandContactEditName $commandEntity */
    #[Override] protected function toEventDataEntity(CommandEntityInterface $commandEntity): EventDataInterface
    {
        return (new EventContactEditName())
            ->setId($commandEntity->getId())
            ->setName($commandEntity->getName());
    }

    #[Override] protected function getResponse(EventEntity $eventEntity): Response
    {
        /** @var EventContactEditName $eventData */
        $eventData = $eventEntity->getEventData();

        return $this->redirectToRoute('contact_detail', ['id' => $eventData->getId()]);
    }

    /**
     * @throws Exception
     */
    #[Override] protected function onFormError(Request $request, FormInterface $form, HttpKernelInterface $httpKernel): Response
    {
        return $this->handelSubController($request, $form, $httpKernel, QueryContactDetailController::class, [
            'id' => $form->getData()->getId()
        ]);
    }
}
