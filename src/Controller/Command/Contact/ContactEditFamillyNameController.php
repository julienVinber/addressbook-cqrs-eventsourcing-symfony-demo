<?php

namespace App\Controller\Command\Contact;

use App\Controller\Query\Contact\QueryContactDetailController;
use App\CQRS\Attribute\AsCommandController;
use App\CQRS\Controller\AbstractCommandController;
use App\CQRS\Entity\Command\CommandEntityInterface;
use App\CQRS\Entity\Event\EventDataInterface;
use App\CQRS\Entity\Event\EventEntity;
use App\Entity\Command\Contact\CommandContactEditFamillyName;
use App\Entity\Event\Contact\EventContactEditFamillyName;
use App\Form\Type\Contact\ContactEditFamillyNameType;
use App\Security\Voter\ContactExistVoter;
use Exception;
use Override;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Attribute\Route;

#[AsCommandController(commandClassName: CommandContactEditFamillyName::class, formTypeClassName: ContactEditFamillyNameType::class)]
#[Route('/contact/edit/famillyName', name: 'contact_edit_famillyName', methods: 'POST')]
class ContactEditFamillyNameController extends AbstractCommandController
{
    /** @param CommandContactEditFamillyName $commandEntity */
    #[Override] protected function isAuthorized(CommandEntityInterface $commandEntity): bool
    {
        return $this->isGranted(ContactExistVoter::CONTACT_EXIST, $commandEntity->getId());
    }

    /** @param CommandContactEditFamillyName $commandEntity */
    #[Override] protected function toEventDataEntity(CommandEntityInterface $commandEntity): EventDataInterface
    {
        return (new EventContactEditFamillyName())
            ->setId($commandEntity->getId())
            ->setFamillyName($commandEntity->getFamillyName());
    }

    #[Override] protected function getResponse(EventEntity $eventEntity): Response
    {
        /** @var EventContactEditFamillyName $eventData */
        $eventData = $eventEntity->getEventData();

        return $this->redirectToRoute('contact_detail', ['id' => $eventData->getId()]);
    }

    /**
     * @throws Exception
     */
    #[Override] protected function onFormError(Request $request, FormInterface $form, HttpKernelInterface $httpKernel): Response
    {
        return $this->handelSubController($request, $form, $httpKernel, QueryContactDetailController::class, [
            'id' => $form->getData()->getId()
        ]);
    }
}
