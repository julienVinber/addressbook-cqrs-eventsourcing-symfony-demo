<?php

namespace App\Controller\Command\Contact;

use App\CQRS\Attribute\AsCommandController;
use App\CQRS\Controller\AbstractCommandController;
use App\CQRS\Entity\Command\CommandEntityInterface;
use App\CQRS\Entity\Event\EventDataInterface;
use App\CQRS\Entity\Event\EventEntity;
use App\Entity\Command\Contact\CommandContactDelete;
use App\Entity\Event\Contact\EventContactDelete;
use App\Form\Type\Contact\ContactDeleteType;
use App\Security\Voter\ContactExistVoter;
use Override;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[AsCommandController(commandClassName: CommandContactDelete::class, formTypeClassName: ContactDeleteType::class)]
#[Route('/contact/delete', name: 'contact_delete', methods: 'POST')]
class ContactDeleteController extends AbstractCommandController
{
    /** @param CommandContactDelete $commandEntity */
    #[Override] protected function isAuthorized(CommandEntityInterface $commandEntity): bool
    {
        return $this->isGranted(ContactExistVoter::CONTACT_EXIST, $commandEntity->getId());
    }

    /** @param CommandContactDelete $commandEntity */
    #[Override] protected function toEventDataEntity(CommandEntityInterface $commandEntity): EventDataInterface
    {
        return (new EventContactDelete())
            ->setId($commandEntity->getId());
    }

    #[Override] protected function getResponse(EventEntity $eventEntity): Response
    {
        return $this->redirectToRoute('home');
    }
}
