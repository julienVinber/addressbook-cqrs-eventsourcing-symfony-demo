<?php

namespace App\Controller\Command\Contact;

use App\Controller\Query\Contact\QueryContactDetailController;
use App\CQRS\Attribute\AsCommandController;
use App\CQRS\Controller\AbstractCommandController;
use App\CQRS\Entity\Command\CommandEntityInterface;
use App\CQRS\Entity\Event\EventDataInterface;
use App\CQRS\Entity\Event\EventEntity;
use App\Entity\Command\Contact\CommandContactEditEmail;
use App\Entity\Event\Contact\EventContactEditEmail;
use App\Form\Type\Contact\ContactEditEmailType;
use App\Security\Voter\ContactExistVoter;
use Exception;
use Override;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Attribute\Route;

#[AsCommandController(commandClassName: CommandContactEditEmail::class, formTypeClassName: ContactEditEmailType::class)]
#[Route('/contact/edit/email', name: 'contact_edit_email', methods: 'POST')]
class ContactEditEmailController extends AbstractCommandController
{
    /** @param CommandContactEditEmail $commandEntity */
    #[Override] protected function isAuthorized(CommandEntityInterface $commandEntity): bool
    {
        return $this->isGranted(ContactExistVoter::CONTACT_EXIST, $commandEntity->getId());
    }

    /** @param CommandContactEditEmail $commandEntity */
    #[Override] protected function toEventDataEntity(CommandEntityInterface $commandEntity): EventDataInterface
    {
        return (new EventContactEditEmail())
            ->setId($commandEntity->getId())
            ->setEmail($commandEntity->getEmail());
    }

    #[Override] protected function getResponse(EventEntity $eventEntity): Response
    {
        /** @var EventContactEditEmail $eventData */
        $eventData = $eventEntity->getEventData();

        return $this->redirectToRoute('contact_detail', ['id' => $eventData->getId()]);
    }

    /**
     * @throws Exception
     */
    #[Override] protected function onFormError(Request $request, FormInterface $form, HttpKernelInterface $httpKernel): Response
    {
        return $this->handelSubController($request, $form, $httpKernel, QueryContactDetailController::class, [
            'id' => $form->getData()->getId()
        ]);
    }
}
