<?php

namespace App\Form\Type\Contact;

use App\Entity\Command\Contact\CommandContactEditGivenName;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UuidType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactEditGivenNameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', UuidType::class, ['attr' => ['class' => 'd-none'], 'label_attr' => ['class' => 'd-none']])
            ->add('givenName', TextType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CommandContactEditGivenName::class,
        ]);
    }
}
