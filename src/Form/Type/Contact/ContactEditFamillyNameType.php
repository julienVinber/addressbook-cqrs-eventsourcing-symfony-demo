<?php

namespace App\Form\Type\Contact;

use App\Entity\Command\Contact\CommandContactEditFamillyName;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UuidType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactEditFamillyNameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', UuidType::class, ['attr' => ['class' => 'd-none'], 'label_attr' => ['class' => 'd-none']])
            ->add('famillyName', TextType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CommandContactEditFamillyName::class,
        ]);
    }
}
