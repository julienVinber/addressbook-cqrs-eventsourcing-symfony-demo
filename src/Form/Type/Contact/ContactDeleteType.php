<?php

namespace App\Form\Type\Contact;

use App\Entity\Command\Contact\CommandContactDelete;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\UuidType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactDeleteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('id', UuidType::class, ['attr' => ['class' => 'd-none'], 'label_attr' => ['class' => 'd-none']]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CommandContactDelete::class,
        ]);
    }
}
