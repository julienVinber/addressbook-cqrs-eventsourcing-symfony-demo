<?php

namespace App\EventHandler;

use App\CQRS\Entity\Event\EventEntity;
use App\CQRS\EventHandler\EventHandlerInterface;
use App\Entity\Event\Contact\EventContactEditFamillyName;
use App\Repository\Query\ContactRepository;
use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;

class ContactEditFamillyNameHandler implements EventHandlerInterface
{
    public function __construct(
        private readonly ContactRepository $contactRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function handle(EventEntity $eventEntity): void
    {
        /** @var EventContactEditFamillyName $data */
        $data = $eventEntity->getEventData();
        $contact = $this->contactRepository->find($data->getId());

        if ($contact === null) {
            throw new RuntimeException('Contact not found in Event.');
        }

        $contact->setFamillyName($data->getFamillyName());

        $this->entityManager->flush();
    }

    public function supported(EventEntity $eventEntity): bool
    {
        return $eventEntity->getEventClassName() === EventContactEditFamillyName::class;
    }
}
