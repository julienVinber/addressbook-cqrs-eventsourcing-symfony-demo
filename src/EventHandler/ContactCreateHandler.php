<?php

namespace App\EventHandler;

use App\CQRS\Entity\Event\EventEntity;
use App\CQRS\EventHandler\EventHandlerInterface;
use App\Entity\Event\Contact\EventContactCreate;
use App\Entity\Query\Contact;
use Doctrine\ORM\EntityManagerInterface;

class ContactCreateHandler implements EventHandlerInterface
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function handle(EventEntity $eventEntity): void
    {
        /** @var EventContactCreate $data */
        $data = $eventEntity->getEventData();

        if ($data->getFamillyName() !== null || $data->getGivenName() === null) {
            $sortName = $data->getFamillyName().' '.$data->getGivenName();
        } else {
            $sortName = $data->getName();
        }

        $contact = (new Contact())
            ->setId($data->getId())
            ->setName($data->getName())
            ->setGiveName($data->getGivenName())
            ->setFamillyName($data->getFamillyName())
            ->setSortName($sortName)
            ->setAddress($data->getAddress())
            ->setPostalCode($data->getPostalCode())
            ->setCity($data->getCity())
            ->setEmail($data->getEmail())
            ->setPhone($data->getPhone());

        $this->entityManager->persist($contact);

        $this->entityManager->flush();
    }

    public function supported(EventEntity $eventEntity): bool
    {
        return $eventEntity->getEventClassName() === EventContactCreate::class;
    }
}
