<?php

namespace App\EventHandler;

use App\CQRS\Entity\Event\EventEntity;
use App\CQRS\EventHandler\EventHandlerInterface;
use App\Entity\Event\Contact\EventContactEditGivenName;
use App\Repository\Query\ContactRepository;
use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;

class ContactGivenNameHandler implements EventHandlerInterface
{
    public function __construct(
        private readonly ContactRepository $contactRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function handle(EventEntity $eventEntity): void
    {
        /** @var EventContactEditGivenName $data */
        $data = $eventEntity->getEventData();
        $contact = $this->contactRepository->find($data->getId());

        if ($contact === null) {
            throw new RuntimeException('Contact not found in Event.');
        }

        $contact->setGiveName($data->getGivenName());

        $this->entityManager->flush();
    }

    public function supported(EventEntity $eventEntity): bool
    {
        return $eventEntity->getEventClassName() === EventContactEditGivenName::class;
    }
}
