<?php

namespace App\CQRS\Command;

use App\CQRS\Repository\Event\EventEntityRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

#[AsCommand(name: 'app:event:load')]
class EventLoadCommand extends Command
{
    public function __construct(
        private readonly EventEntityRepository $eventEntityRepository,
        private readonly EventDispatcherInterface $dispatcher,
        string $name = null
    ) {
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Chargement des Evénements');
        $events = $this->eventEntityRepository->findAll();

        foreach ($events as $event) {
            $this->dispatcher->dispatch($event);
        }

        return Command::SUCCESS;
    }
}
