<?php

namespace App\CQRS\Repository\Command;

use App\CQRS\Entity\Command\EventEntityStored;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<EventEntityStored>
 *
 * @method EventEntityStored|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventEntityStored|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventEntityStored[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventEntityStoredRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventEntityStored::class);
    }

    public function add(EventEntityStored $eventEntityStored, bool $flush = true): void
    {
        $this->_em->persist($eventEntityStored);

        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @return EventEntityStored[]
     */
    public function findAll(): array
    {
        return $this->findBy([], ['dateEvent' => 'ASC']);
    }
}
