<?php

namespace App\CQRS\Repository\Event;

use App\CQRS\Entity\Command\EventEntityStored;
use App\CQRS\Entity\Event\EventDataInterface;
use App\CQRS\Entity\Event\EventEntity;
use App\CQRS\Repository\Command\EventEntityStoredRepository;
use Generator;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\Uuid;

class EventEntityRepository
{
    public function __construct(
        private readonly EventEntityStoredRepository $entityStoredRepository,
        private readonly SerializerInterface         $serializer
    ) {
    }

    public function add(EventEntity $eventEntity, bool $flush = true): void
    {
        $this->entityStoredRepository->add($this->normalize($eventEntity), $flush);
    }

    public function find(Uuid $id): ?EventEntity
    {
        $eventEntityStored = $this->entityStoredRepository->find($id);

        return $eventEntityStored !== null ? $this->denormalize($eventEntityStored) : null;
    }

    /**
     * @return Generator<EventEntity>
     */
    public function findAll(): Generator
    {
        $events = $this->entityStoredRepository->findAll();
        foreach ($events as $event) {
            yield $this->denormalize($event);
        }
    }

    private function normalize(EventEntity $eventEntity): EventEntityStored
    {
        return new EventEntityStored(
            $eventEntity->getId(),
            $eventEntity->getDateEvent(),
            $eventEntity->getIdUser(),
            $eventEntity->getEventClassName(),
            $this->serializer->serialize($eventEntity->getEventData(), 'json'),
        );
    }

    private function denormalize(EventEntityStored $eventEntityStored): EventEntity
    {
        /** @var EventDataInterface $data */
        $data = $this->serializer->deserialize($eventEntityStored->getEventData(), $eventEntityStored->getEventClassName(), 'json');

        return new EventEntity(
            $eventEntityStored->getId(),
            $eventEntityStored->getDateEvent(),
            $eventEntityStored->getIdUser(),
            $data,
        );
    }
}
