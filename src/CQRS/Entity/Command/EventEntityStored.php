<?php

namespace App\CQRS\Entity\Command;

use App\CQRS\Repository\Command\EventEntityStoredRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: EventEntityStoredRepository::class)]
#[ORM\Table(name: 'event')]
final readonly class EventEntityStored
{
    public function __construct(
        #[ORM\Id]
        #[ORM\Column(type: 'uuid')]
        private Uuid              $id,
        #[ORM\Column(type: 'datetime_immutable')]
        private DateTimeImmutable $dateEvent,
        #[ORM\Column(type: 'uuid')]
        private Uuid              $idUser,
        #[ORM\Column(type: 'string')]
        private string            $eventClassName,
        #[ORM\Column(type: 'text')]
        private string            $eventData
    ) {
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getIdUser(): Uuid
    {
        return $this->idUser;
    }

    public function getDateEvent(): DateTimeImmutable
    {
        return $this->dateEvent;
    }

    public function getEventClassName(): string
    {
        return $this->eventClassName;
    }

    public function getEventData(): string
    {
        return $this->eventData;
    }
}
