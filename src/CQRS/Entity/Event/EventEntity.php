<?php

namespace App\CQRS\Entity\Event;

use DateTimeImmutable;
use Symfony\Component\Uid\Uuid;

final readonly class EventEntity
{
    public function __construct(
        private Uuid               $id,
        private DateTimeImmutable  $dateEvent,
        private Uuid               $idUser,
        private EventDataInterface $eventData
    ) {
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getIdUser(): Uuid
    {
        return $this->idUser;
    }

    public function getDateEvent(): DateTimeImmutable
    {
        return $this->dateEvent;
    }

    public function getEventClassName(): string
    {
        return get_class($this->getEventData());
    }

    public function getEventData(): EventDataInterface
    {
        return $this->eventData;
    }
}
