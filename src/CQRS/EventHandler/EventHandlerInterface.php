<?php

namespace App\CQRS\EventHandler;

use App\CQRS\Entity\Event\EventEntity;

interface EventHandlerInterface
{
    public function handle(EventEntity $eventEntity): void;

    public function supported(EventEntity $eventEntity): bool;
}
