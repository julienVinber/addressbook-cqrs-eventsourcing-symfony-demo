<?php

namespace App\CQRS\Attribute;

use Attribute;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[Attribute(Attribute::TARGET_CLASS | Attribute::TARGET_FUNCTION)]
class AsCommandController extends AsController
{
    public function __construct(
        public string $commandClassName,
        public string $formTypeClassName,
    )
    {
        parent::__construct();
    }
}
