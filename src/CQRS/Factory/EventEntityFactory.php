<?php

namespace App\CQRS\Factory;

use App\CQRS\Entity\Event\EventDataInterface;
use App\CQRS\Entity\Event\EventEntity;
use DateTimeImmutable;
use Symfony\Component\Uid\Uuid;

class EventEntityFactory
{
    private const ID_APP_STRING = '2596ff58-78c4-11ee-b962-0242ac120002';
    private static ?Uuid $id_app = null;

    public static function createEvent(EventDataInterface $eventData, Uuid $idUser = null): EventEntity
    {
        return new EventEntity(
            Uuid::v7(),
            new DateTimeImmutable(),
            $idUser ?? self::getIdApp(),
            $eventData
        );
    }

    public static function getIdApp(): Uuid
    {
        if (self::$id_app === null) {
            self::$id_app = Uuid::fromString(self::ID_APP_STRING);
        }

        return self::$id_app;
    }
}
