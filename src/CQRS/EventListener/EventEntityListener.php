<?php

namespace App\CQRS\EventListener;

use App\CQRS\Entity\Event\EventEntity;
use App\CQRS\EventHandler\EventHandlerInterface;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[AsEventListener]
class EventEntityListener
{
    /**
     * @param iterable<EventHandlerInterface> $handlers
     */
    public function __construct(private readonly iterable $handlers)
    {
    }

    public function __invoke(EventEntity $event): void
    {
        $handler = $this->getHandler($event);

        if ($handler === null) {
            return;
        }

        $handler->handle($event);
    }

    public function getHandler(EventEntity $event): ?EventHandlerInterface
    {
        /** @var EventHandlerInterface $handler */
        foreach ($this->handlers as $handler) {
            if ($handler->supported($event)) {
                return $handler;
            }
        }

        return null;
    }
}
