<?php

namespace App\CQRS\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class AbstractQueryController extends AbstractController
{
}
