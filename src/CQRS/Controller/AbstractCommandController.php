<?php

namespace App\CQRS\Controller;

use App\Controller\Query\Contact\QueryContactDetailController;
use App\CQRS\Attribute\AsCommandController;
use App\CQRS\Entity\Command\CommandEntityInterface;
use App\CQRS\Entity\Event\EventDataInterface;
use App\CQRS\Entity\Event\EventEntity;
use App\CQRS\Factory\EventEntityFactory;
use App\CQRS\Repository\Event\EventEntityRepository;
use Exception;
use ReflectionClass;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;

abstract class AbstractCommandController extends AbstractController
{

    protected string $commandClassName;
    protected string $formTypeClassName;

    public function __construct()
    {
        $reflexionAttibuttes = (new ReflectionClass($this))->getAttributes(AsCommandController::class);

        if (count($reflexionAttibuttes) === 0) {
            throw new RuntimeException(self::class . ' must have AsCommandController attribute');
        }

        $attributeArguments = $reflexionAttibuttes[0]->getArguments();
        $this->commandClassName = $attributeArguments['commandClassName'];
        $this->formTypeClassName = $attributeArguments['formTypeClassName'];
    }

    public function __invoke(
        Request                  $request,
        EventDispatcherInterface $dispatcher,
        EventEntityFactory       $eventEntityFactory,
        EventEntityRepository    $eventEntityRepository,
        HttpKernelInterface      $httpKernel
    ): Response
    {
        /** @var CommandEntityInterface $commandEntity */
        $commandEntity = new $this->commandClassName();

        $form = $this->createForm($this->formTypeClassName, $commandEntity);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return $this->onFormError($request, $form, $httpKernel);
        }

        if (!$this->isValid()) {
            throw new BadRequestHttpException('Invalid data');
        }

        if (!$this->isAuthorized($commandEntity)) {
            throw new AccessDeniedHttpException();
        }

        $event = $eventEntityFactory->createEvent($this->toEventDataEntity($commandEntity));

        $eventEntityRepository->add($event);

        $dispatcher->dispatch($event);

        return $this->getResponse($event);
    }

    protected function isValid(): bool
    {
        return true;
    }

    protected function isAuthorized(CommandEntityInterface $commandEntity): bool
    {
        return true;
    }

    abstract protected function toEventDataEntity(CommandEntityInterface $commandEntity): EventDataInterface;

    abstract protected function getResponse(EventEntity $eventEntity): Response;

    protected function onFormError(Request $request, FormInterface $form, HttpKernelInterface $httpKernel): Response
    {
        throw new BadRequestHttpException('Invalid data');
    }

    /**
     * @throws Exception
     */
    protected function handelSubController(Request $request, FormInterface $form, HttpKernelInterface $httpKernel, string $controller, array $attributes = []): Response
    {
        $subAttributes = [...$request->attributes->all(), ...$attributes];
        $subAttributes['_controller'] = QueryContactDetailController::class;
        $subRequest = $request->duplicate(null, null, $subAttributes);
        return $httpKernel->handle($subRequest, HttpKernelInterface::SUB_REQUEST);
    }
}
