<?php

namespace App\Security\Voter;

use App\Entity\Query\Contact;
use App\Repository\Query\ContactRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Uid\Uuid;

/**
 * @phpstan-extends Voter<'contactExist', Contact|Uuid>
 */
class ContactExistVoter extends Voter
{
    public const string CONTACT_EXIST = 'contactExist';

    public function __construct(private readonly ContactRepository $contactRepository)
    {
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        return $attribute === self::CONTACT_EXIST && ($subject instanceof Contact || $subject instanceof Uuid);
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        if ($subject instanceof Contact) {
            $id = $subject->getId();
        } else {
            $id = $subject;
        }

        return $this->contactRepository->find($id) !== null;
    }
}
