# AddressBook

AddressBook est un projet qui a pour but de montrer comment il est possible de mettre en place CQRS et Event Sourcing
dans un projet Symfony.

Attention : c'est juste une démo technique, il ne faut pas prendre le code présenté comme solution à utiliser dans vos
projets sans l'adapter à vos besoins.

## Exécuter le projet

On peut utiliser Docker pour faire fonctionner le projet en local

### Lancement normal

``` shell
bin/dockerUp
```

### Lancer en réinitialisant les bases de données

``` shell
bin/dockerCleanData
```

### Lancer en reconstruisant intégralement les conteneurs

``` shell
bin/dockerRecreate
```


## Init complète

Permets de tout réinitialiser.

``` shell
bin/init
```

## Init Query uniquement

Détruit uniquement les bases Query pour les recréer à partir des Events.

``` shell
bin/rebuildQueryData
```

## Ajouter une commande :

* Créer une `Entity\Command`
* Créer un `Entity\Event`
* Créer un `EventHandler`
* Créer un `Form\Type`
* Créer un `Controller\Command`
* Il est surement nécessaire de créer une fonction de transformation pour récupérer `Entity\Command`,
  `voir App\Factory\QueryToCommandContactFactory`

=> exploiter le formulaire

## Ajouter une Query :

C'est du Symfony normal, mais en ne passant que par des classes faisant référence à Query.

Il faut juste penser à modifier les Handlers si nécessaire

## Qualité

``` shell
docker exec -it adressbook_php php /app/php vendor/bin/php-cs-fixer fix
docker exec -it adressbook_php php /app/php vendor/bin/phpstan analyse
```
